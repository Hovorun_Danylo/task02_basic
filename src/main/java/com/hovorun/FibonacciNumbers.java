package com.hovorun;

public class FibonacciNumbers {
    static int f1;
    static int f2;

    public static int fibonacci(final int upperNumber, final int startingNumber) {
        if (upperNumber == startingNumber) {
            return startingNumber;
        } else if (upperNumber == 0) {
            return 0;
        } else if (upperNumber == 1) {
            return 1;
        } else {
            return fibonacci(upperNumber - 1, startingNumber)
                    + fibonacci(upperNumber - 2, startingNumber);
        }
    }
}

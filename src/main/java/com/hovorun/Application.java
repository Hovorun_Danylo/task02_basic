package com.hovorun;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int startOfInterval = scanner.nextInt();
        int endOfInterval = scanner.nextInt();
        int sumOfOddNumbers = 0;
        int sumOfEvenNumbers = 0;

        System.out.println(startOfInterval + "; " + endOfInterval);

        for (int i = startOfInterval; i < endOfInterval; i++) {
            if (i % 2 != 0) {
                sumOfOddNumbers += i;
                System.out.println(i + " ");
            } else {
                sumOfEvenNumbers += i;
            }
        }

        System.out.println("Sum of odd numbers: " + sumOfOddNumbers);
        System.out.println("Sum of even numbers: " + sumOfEvenNumbers);

        System.out.println("Fibonacci numbers: "
                + FibonacciNumbers.fibonacci(endOfInterval, startOfInterval));

    }

}
